const express = require('express');
const EmployeesService = require('../services/employees');

function employeesApi(app) {
  const router = express.Router();
  app.use('/api/employees', router)

  const employeesService = new EmployeesService();

  // Lista de completa de empleados
  router.get('/', async function (req, res, next) {
    try {
      const employees = await employeesService.getEmployees()
      res.status(200).json({
        data: employees,
        message: 'employees listed'
      })
    } catch (error) {
      next(error);
    }
  });

  // Un empleado
  router.get('/:employeeId', async function (req, res, next) {
    const { employeeId } = req.params;
    try {
      const employee = await employeesService.getEmployee({ employeeId })
      res.status(200).json({
        data: employee,
        message: 'employee retrieved'
      })
    } catch (error) {
      next(error);
    }
  });

  router.post('/', async function (req, res, next) {
    const { body: data } = req;
    try {
      const createdEmployeeId = await employeesService.createEmployee({ data })
      res.status(201).json({
        data: createdEmployeeId,
        message: 'employee created'
      })
    } catch (error) {
      next(error);
    }
  });

  router.put('/:employeeId', async function (req, res, next) {
    const { employeeId } = req.params;
    const { body: data } = req;
    try {
      const updatedEmployeeId = await employeesService.updateEmployee({ employeeId, data })
      res.status(200).json({
        data: updatedEmployeeId,
        message: 'employee updated'
      })
    } catch (error) {
      next(error);
    }
  });

  router.delete('/:employeeId', async function (req, res, next) {
    const { employeeId } = req.params;
    try {
      const deletedEmployeeId = await employeesService.deleteEmployee({ employeeId })
      res.status(200).json({
        data: deletedEmployeeId,
        message: 'employee deleted'
      })
    } catch (error) {
      next(error);
    }
  });
}

module.exports = employeesApi;
