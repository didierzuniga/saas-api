const { Pool } = require('pg');
const { config } = require('../config');
const { createQuery, updateQuery, deleteQuery } = require('./queries');

const USER = encodeURIComponent(config.dbUser);
const PASSWORD = encodeURIComponent(config.dbPassword);
const DB_HOST = config.dbHost;
const DB_PORT = config.dbPort;
const DB_NAME = config.dbName;

// const POSTGRES_URI = `localhost`;

class PostgresLib {
  constructor() {
    this.pool = new Pool({
      user: USER,
      host: DB_HOST,
      password: PASSWORD,
      database: DB_NAME
    })
    this.dbName = DB_NAME;
  }

  connect() {
    if (!PostgresLib.connection) {
      PostgresLib.connection = new Promise((resolve, reject) => {
        this.pool.connect(err => {
          if (err) {
            reject(err);
          }
          console.log('Connected succesfully to Postgres');
          resolve(this.pool.db(this.dbName))
        })
      })
    }

    return PostgresLib.connection;
  }

  getAll(table) {
    const query = {
      name: 'get-all-employees',
      text: `SELECT * FROM ${table}`
    }
    return this.pool
      .query(query)
      .then(res => res.rows)
      .catch(e => console.error(e.stack))
  }

  get(table, id) {
    const query = {
      name: 'get-employee',
      text: `SELECT * FROM ${table} WHERE id = $1`,
      values: [id]
    }
    return this.pool
      .query(query)
      .then(res => res.rows)
      .catch(e => e.stack)
  }

  create(table, data) {
    const queryResult = createQuery(table, data);
    const query = {
      text: queryResult.text,
      values: queryResult.values
    }
    return this.pool
      .query(query)
      .then(res => res.rows[0].id)
      .catch(e => e.stack)
  }

  update(table, id, data) {
    const queryResult = updateQuery(table, id, data);
    const query = {
      text: queryResult.text
    }
    return this.pool
      .query(query)
      .then(res => res.rows[0].id)
      .catch(e => e.stack)
  }

  delete(table, id) {
    const queryResult = deleteQuery(table, id);
    const query = {
      text: queryResult.text
    }
    return this.pool
      .query(query)
      .then(res => res.rows[0].id)
      .catch(e => e.stack)
  }

}

module.exports = PostgresLib;
