const createQuery = (table, data) => {
  let fields = '';
  const values = [];
  let indexValues = '';

  const fieldsArray = [];
  const indexValuesArray = [];
  const keys = Object.keys(data);
  keys.forEach(function (key, i) {
    fieldsArray.push(key);
    values.push(data[key]);
    indexValuesArray.push('$' + (i + 1));
  });
  fields = fieldsArray.toString();
  indexValues = indexValuesArray.toString();

  return {
    text: `INSERT INTO ${table} (${fields}) VALUES (${indexValues}) RETURNING id`,
    values
  }
}

const updateQuery = (table, id, data) => {
  const fieldsValuesIndexArray = [];
  const values = [];
  const keys = Object.keys(data);
  keys.forEach(function (key, i) {
    if (typeof data[key] === 'string') {
      fieldsValuesIndexArray.push(' ' + key + ' = ' + '\'' + data[key] + '\'');
    } else {
      fieldsValuesIndexArray.push(' ' + key + ' = ' + data[key]);
    }

    values.push(data[key]);
  });
  const fieldsValuesIndexText = fieldsValuesIndexArray.toString();
  return {
    text: `UPDATE ${table} SET ${fieldsValuesIndexText} WHERE id=${id} RETURNING id`
  }
}

const deleteQuery = (table, id) => {
  return {
    text: `DELETE FROM ${table} WHERE id=${id} RETURNING id`
  }
}

module.exports = {
  createQuery,
  updateQuery,
  deleteQuery
};
