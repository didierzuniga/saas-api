const PostgresLib = require('../lib/postgres');

class EmployeesService {
  constructor() {
    this.table = 'employee';
    this.postgresDB = new PostgresLib();
  }

  async getEmployees() {
    const employees = await this.postgresDB.getAll(this.table);
    return employees || [];
  };

  async getEmployee({ employeeId }) {
    const employee = await this.postgresDB.get(this.table, employeeId);
    return employee || {};
  };

  async createEmployee({ data }) {
    const createdEmployeeId = await this.postgresDB.create(this.table, data);
    return createdEmployeeId || {};
  };

  async updateEmployee({ employeeId, data }) {
    const updatedEmployeeId = await this.postgresDB.update(this.table, employeeId, data);
    return updatedEmployeeId || {};
  };

  async deleteEmployee({ employeeId }) {
    const deletedEmployeeId = await this.postgresDB.delete(this.table, employeeId);
    return deletedEmployeeId || {};
  };

};

module.exports = EmployeesService;
